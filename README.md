# README #

This command line utility will allow you to connect to a SOLIDWORKS Workgroup PDM server to extract the documents contained within it. The need for this application arose during our migration from Workgroup PDM to PDM Standard. Using this utility you can copy either just the latest or all revisions of every document to a plain folder structure.

### Caveats ###
* This is a VERY basic program designed to meet our very basic needs. 
* It will not retain any metadata from the Workgroup PDM database. No user permissions, nothing.
* All it does is copy the files out into a folder structure that mirrors your project structure.

### Warnings ###
Please (PLEASE!) do not run this on a production database. I recommend creating a new instance of the Workgroup PDM server on your local computer and restore your latest backup into it. When the time comes to perform the migration, create a backup of your production database, restore to your copy, and run the script there. This will always ensure that no harm comes to your data. That being said, there is no functionality in this program that could possibly alter your data in anyway. So it's unlikely anything could go wrong although it should run MUCH faster if operating on a local database instead of one over a network.

### Requirements ###
* SOLIDWORKS Workgroup PDM API installed on local machine
* Visual Studio 2010 or newer (the included solution file is for 2010, although newer should work)

### Who do I talk to? ###
Feel free to contact Devin at devin@keoscientific.com if you have questions. I set out to create this program to avoid the costly fees of our VAR. If this program can help anyone else then that is great!