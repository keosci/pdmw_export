﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PDMWorks;
using CommandLine;
using CommandLine.Text;

namespace PDMW_Export
{
    class Options
    {
        [Option('s', "server", DefaultValue = "localhost", HelpText = "Address for Workgroup PDM Server")]
        public string Server { get; set; }

        [Option('u', "username", Required = true, HelpText = "Username for Workgroup PDM Server.")]
        public string Username { get; set; }

        [Option('p', "password", Required = true, HelpText = "Password for Workgroup PDM Server.")]
        public string Password { get; set; }

        [Option('o', "output", Required = true, HelpText = "Output root directory.")]
        public string OutputDir { get; set; }

        [Option('c', "copy", DefaultValue = false, HelpText = "Copy the files out of the vault (False = simulate).")]
        public bool Copy { get; set; }

        [Option('a', "allRev", DefaultValue = false, HelpText = "Copy all revisions, not just latest.")]
        public bool AllRevisions { get; set; }

        [Option('v', "verbose", DefaultValue = false, HelpText = "Prints additional information to standard output.")]
        public bool Verbose { get; set; }

        [Option('f', "file", HelpText = "Filename to output console to.")]
        public string file { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            StreamWriter sw = StreamWriter.Null;
            TextWriter tmp = Console.Out;

            Console.Clear();
            Console.WriteLine("SOLIDWORKS Workgroup PDM Export Tool");
            Console.WriteLine("Copyright (c) 2016 Keo Scientific Ltd.");
            Console.WriteLine("Written by Devin Wyatt (devin@keoscientific.com)");
            Console.WriteLine("================================================");

            // parse the command line
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                // tweak some input parameters

                // check that output path doesn't have '\' at end
                options.OutputDir = options.OutputDir.TrimEnd('\\');

                if (options.Verbose)
                {
                    Console.WriteLine("\nCommandline Parameters:");
                    Console.WriteLine("\tServer = {0}",options.Server);
                    Console.WriteLine("\tUsername = {0}",options.Username);
                    Console.WriteLine("\tPassword = {0}",options.Password);
                    Console.WriteLine("\tOutput = {0}", options.OutputDir);
                    Console.WriteLine("\tCopy = {0}", options.Copy);
                    Console.WriteLine("\tAllRev = {0}", options.AllRevisions);
                    Console.WriteLine("\tVerbose = {0}",options.Verbose);
                    Console.WriteLine("\tFile = {0}", options.file);
                }

                // redirect further output to file
                if (options.file != null)
                {
                    Console.WriteLine("All further output being redirected to \"{0}\".", options.file);
                    FileStream fs = new FileStream(options.file, FileMode.Create);
                    sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                }
            }

            // errors parsing command line
            else
            {
                return;
            }

            // create connection
            var connection = new PDMWConnection();

            // connect to server
            if (options.Verbose) Console.WriteLine("Connecting to server \"{0}\" with username \"{1}\" and password \"{2}\"",options.Server,options.Username,options.Password);
            if (0 == connection.Login(options.Username, options.Password, options.Server) )
            {
                if (options.Verbose) Console.WriteLine("Successfully connected to server \"{0}\".",options.Server);
                
                // get all the documents in the vault
                if (options.Verbose)
                {
                    var allDocs = connection.Documents;
                    Console.WriteLine("\nTotal number of documents in vault = {0}", allDocs.Count);
                }

                // list to hold all parents of a given project
                List<string> parents = new List<string>();
                ProjectsProcessor projectProcessor = new ProjectsProcessor();

                // set options
                projectProcessor.options = options;

                Console.WriteLine("\nStarting to process projects.");

                // for each top level project, process
                var allProjects = connection.Projects;
                foreach (IPDMWProject project in allProjects)
                {
                    // filter out the projects that aren't at root level
                    if (project.get_Parent() != null)
                        continue;

                    // filter out library components (toolbox, design library references, etc)
                    if (project.Name.ToLower() == "library components")
                        continue;

                    // clear list of parents since we are at top level
                    parents.Clear();

                    // get sub-projects of this project
                    projectProcessor.GetSubProjects(0, parents, project);
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Closing connection to server.");
                connection.Logout();
            }
            else
            {
                Console.WriteLine("Error connecting to server.");
            }

            // restore default text writer
            if (options.file != null)
            {
                Console.SetOut(tmp);
                sw.Close();
            }

            Console.WriteLine("Processing complete.");
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }
    }

    class ProjectsProcessor
    {
        public Options options { get; set; }

        public void GetSubProjects(int level,List<string> parents, IPDMWProject parentProject)
        {
            WriteLineNested(level, ConsoleColor.Green, "{0}", parentProject.Name);

            // copy of parent's parents
            // we will add to this as we go down the project tree
            List<string> parentsCopy = new List<string>(parents);

            // add parent to list
            parentsCopy.Add(parentProject.Name);

            // depending on options selected, perform different actions on the current project
            
            // not copying, just iterate through vault and print out all the documents
            if (!options.Copy)
            {
                PrintDocuments(level, parentProject);
            }

            // copying and only latest revisions, save latest revisions
            else if (options.Copy && !options.AllRevisions)
            {
                SaveLatestDocuments(level, parentsCopy, parentProject);
            }

            // copying and all revisions, save all revisions
            else if (options.Copy && options.AllRevisions)
            {
                SaveAllDocuments(level, parentsCopy, parentProject);
            }

            // current projects documents are handled, now repeat for all sub-projects 
            // of this project
            var allSubProjects = parentProject.SubProjects;
            foreach (IPDMWProject subProject in allSubProjects)
            {
                // this is a recursive call to the same function we are already in
                // will continue all the way down the rabbit hole until we find a project
                // with no sub-projects
                GetSubProjects(level + 1, parentsCopy, subProject);
            }
            
        }

        private void PrintDocuments(int level, IPDMWProject project)
        {
            // print out the name of all documents in the project along with revisions
            var allDocuments = project.Documents;
            foreach (IPDMWDocument document in allDocuments)
            {
                WriteLineNested(level + 1, ConsoleColor.Gray, "{0}", document.Name);

                if (options.AllRevisions)
                {
                    // get revisions of the document
                    var allRevisions = document.Revisions;

                    // create a string of all revisions (e.g. "A-01, A-02, B-01")
                    string str = "";
                    foreach (PDMWDocument revision in allRevisions)
                    {
                        str += revision.Revision + ", ";
                    }

                    // there will be an extra ", " at the end, remove it
                    str = str.Remove(str.Length - 2, 2);

                    WriteLineNested(level + 2, ConsoleColor.Yellow, "{0}", str);
                }
            }
        }

        private void SaveLatestDocuments(int level, List<string> parents, IPDMWProject project)
        {
            var allDocuments = project.Documents;

            // format the list of parent projects into a path
            string parentsPath = FormatParentsToPath(parents);

            if (!options.Verbose) WriteLineNested(level + 1, ConsoleColor.Gray, "Saving {0} documents.", allDocuments.Count);

            // iterate through every document in this project and save them
            foreach (IPDMWDocument document in allDocuments)
            {
                // create path for document
                // documents are saved into folders by project
                string documentPath = options.OutputDir + @"\" + parentsPath + @"\";
                if (options.Verbose) WriteLineNested(level + 1, ConsoleColor.Gray, "Saving \"{0}\"", documentPath + document.Name);

                // create the directory
                System.IO.FileInfo file = new System.IO.FileInfo(documentPath);
                file.Directory.Create();

                // save the document (this always saves the latest revision)
                if (0 != document.Save(documentPath))
                {
                    WriteLineNested(level + 1, ConsoleColor.Red, "ERROR SAVING \"{0}\"", documentPath);
                }
            }
        }

        private void SaveAllDocuments(int level, List<string> parents, IPDMWProject project)
        {
            var allDocuments = project.Documents;

            // format the list of parent projects into a path
            string parentsPath = FormatParentsToPath(parents);

            if (!options.Verbose) WriteLineNested(level + 1, ConsoleColor.Gray, "Saving {0} documents with all revisions.", allDocuments.Count);

            // iterate through every document in this project
            foreach (IPDMWDocument document in allDocuments)
            {
                // get revisions of the document and save each of them
                var allRevisions = document.Revisions;
                foreach (PDMWDocument revision in allRevisions)
                {
                    // create path
                    // revisions are appended as a file extension
                    // for example, revision B-05 of a document called "part.sldprt" will be saved in
                    // a folder called "part.sldprt" and with a filename of "part.sldprt.B-05"
                    string documentPath = options.OutputDir + @"\" + parentsPath + @"\" + document.Name + @"\";
                    string documentName = document.Name + "." + revision.Revision;
                    if (options.Verbose) WriteLineNested(level + 1, ConsoleColor.Gray, "Saving \"{0}\"", documentPath + documentName);

                    // create the directory
                    System.IO.FileInfo file = new System.IO.FileInfo(documentPath);
                    file.Directory.Create();

                    // save the revision
                    if (0 == revision.Save(documentPath) )
                    {
                        // rename file to include revision
                        File.Move(documentPath + document.Name, documentPath + documentName);
                    }
                    else
                    {
                        WriteLineNested(level + 1, ConsoleColor.Red, "ERROR SAVING \"{0}\"", documentPath + documentName);
                    }
                }
            }
        }

        private string FormatParentsToPath(List<string> parents)
        {
            // concatenate all parent projects together with '\' in between
            string parentPath = "";
            foreach (string parent in parents)
            {                
                parentPath += parent + @"\";
            }

            // there will be an extra '\' at the end of the path, remove it
            parentPath = parentPath.Remove(parentPath.Length - 1, 1);

            return parentPath.ToString();
        }

        private void WriteLineNested(int level, ConsoleColor color, string format, object arg0)
        {
            // set color of text
            Console.ForegroundColor = color;

            // write some white space for indentation based on level
            for (int i = 0; i < level; i++)
            {
                Console.Write("  ");
            }

            // print the string
            Console.Write(format + "\n", arg0);
        }

    }
}
